import React, { Component } from 'react';

export default class Welcome extends Component {
    render() {
        return (
            <div className="row">
                <div className="col s12">
                    <div className="row">
                        <div className="col s6 offset-s3">
                            <h3>Welcome to the Faculty and Student Team!</h3>
                            <p>
                                Please sign in or create an account to continue.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}