import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';
import { Modal, Col, Card } from 'react-materialize';
import { Link } from 'react-router-dom';

const getCourse = gql`
    query getCourse($id: String!) {
        getCourse(_id: $id) {
            _id
            name
            description
            students {
                uid
                fname
                lname
            }
            comments {
                _id
                author
                comment
            }
        }
    }
`

const addStudent = gql`
    mutation addStudent($id: String!, $fname: String!, $lname: String!, $courseId: String!) {
        addStudent(uid: $id, fname: $fname, lname: $lname, courseId: $courseId) {
            _id
        }
    }
`

const addComment = gql`
    mutation addComment($author: String!, $comment: String!, $courseId: String!) {
        addComment(author: $author, comment: $comment, courseId: $courseId) {
            _id
        }
    }
`

const joinCourse = () => {
    let input;
}

const newComment = () => {
    let input;
}

export default class Homework extends Component {

    render() {
        const { user, client, obj } = this.props;
        const courseId = obj.match.params.id

        return (
            <div>
                <Query query={getCourse} variables={{id: courseId}} pollInterval={1000}>
                {({ data, loading, error, startPolling, stopPolling }) => {
                    if (loading) {
                        return (
                            <div>
                                Loading ...
                            </div>
                        )
                    }

                    if (error) {
                        console.log(error);
                        return (
                            <div>
                                An unexpected error occured.<br/>
                            </div>
                        )
                    }

                    const course = data.getCourse[0]
                    const students = course.students
                    const comments = course.comments
                    if (user.profile.isFaculty) {
                        return (
                            <div>
                            <div className="row">
                                <div className="col s10 offset-s1">
                                    <div className="col">
                                        <Link to={"/course/:id" + course._id}>Back</Link>
                                    </div>
                                </div>
                            </div>
                                <div className="row">
                                    <div className="col s10 offset-s1">
                                        <h3 className="">{course.name}</h3>
                                        <br/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col s10 offset-s1">
                                        <h3>Comments</h3>
                                    </div>
                                    {comments.map(comment => (
                                        <div className="col s10 offset-s1" key={comment._id}>
                                            <Col s={12} className="whitespace">
                                                <Card title={comment.author}>
                                                    {comment.comment}
                                                </Card>
                                            </Col>
                                        </div>
                                    ))}
                                </div>
                            <Mutation mutation={addComment}>
                            {(newComment, { data }) => {
                                var thisAuthor = user.profile.firstName + ' ' + user.profile.lastName
                                return (
                                    <div>
                                        <br/><br/><br/>
                                        <div className="row">
                                            <div className="col s10 offset-s1">
                                                <h4>Join the conversation!</h4>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col s12">
                                                <form onSubmit={e => {
                                                    e.preventDefault();
                                                    newComment({ variables: {author: thisAuthor, comment: thisComment.value, courseId: course._id}})
                                                    console.log(thisAuthor)
                                                    console.log(thisComment.value)
                                                    console.log(courseId)
                                                }}>
                                                    <div className="input-field col s6 offset-s3">
                                                        <textarea id="thisComment" className="materialize-textarea" type="text" ref={input => (this.thisComment = input)}/>
                                                        <label htmlFor="thisComment">Your Comment</label>
                                                    </div>
                                                    <button className="btn col offset-s10" type="submit">Comment</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                )
                            }}
                            </Mutation>
                            </div>
                        )
                    }
                    return (
                        <div>
                            <div className="row">
                                <div className="col s10 offset-s1">
                                    <h3 className="col s5">{course.name}</h3>
                                </div>
                                <div className="col s10 offset-s1">
                                    <p>{course.description}</p>
                                    <Modal header='Registered Students' trigger={<a href='#'>Registered Students</a>}>
                                        <ul>
                                            {students.map(student => (
                                                <li key={student.uid}>{student.fname + ' ' + student.lname}</li>
                                            ))}
                                        </ul>
                                    </Modal>
                                </div>
                            </div>
                            <Mutation mutation={addStudent}>
                            {(joinCourse, { data }) => {
                                for (var i=0; i < students.length; i++) {
                                    if (students[i].uid == user._id) {
                                        return null
                                    }
                                }
                                return (
                                    <div className="row">
                                        <div className="col s10 offset-s1">
                                            <form onSubmit={e => {
                                                e.preventDefault();
                                                joinCourse({ variables: { id:user._id, fname:user.profile.firstName, lname:user.profile.lastName, courseId:courseId }})
                                            }}>
                                            <button className="btn col offset-s10" type="submit">Join</button>
                                            </form>
                                        </div>
                                    </div>
                                )
                            }}
                            </Mutation>
                            <div className="row">
                                <div className="col s10 offset-s1">
                                    <h3>Comments</h3>
                                </div>
                                {comments.map(comment => (
                                    <div className="col s10 offset-s1" key={comment._id}>
                                        <Col s={12} className="whitespace">
                                            <Card title={comment.author}>
                                                {comment.comment}
                                            </Card>
                                        </Col>
                                    </div>
                                ))}
                            </div>
                            <Mutation mutation={addComment}>
                            {(newComment, { data }) => {
                                var thisAuthor = user.profile.firstName + ' ' + user.profile.lastName
                                return (
                                    <div>
                                        <br/><br/><br/>
                                        <div className="row">
                                            <div className="col s10 offset-s1">
                                                <h4>Join the conversation!</h4>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col s12">
                                                <form onSubmit={e => {
                                                    e.preventDefault();
                                                    newComment({ variables: {author: thisAuthor, comment: thisComment.value, courseId: course._id}})
                                                    console.log(thisAuthor)
                                                    console.log(thisComment.value)
                                                    console.log(courseId)
                                                }}>
                                                    <div className="input-field col s6 offset-s3">
                                                        <textarea id="thisComment" className="materialize-textarea" type="text" ref={input => (this.thisComment = input)}/>
                                                        <label htmlFor="thisComment">Your Comment</label>
                                                    </div>
                                                    <button className="btn col offset-s10" type="submit">Comment</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                )
                            }}
                            </Mutation>
                        </div>
                    )
                }}
                </Query>
            </div>
        )
       return null;
    }
}