import Student from './student';

export default {
    Mutation: {
        addStudent(obj, { uid, fname, lname, courseId }, context) {
            const studentId = Student.insert({
                uid,
                fname,
                lname,
                courseId
            });
            return Student.findOne(studentId)
        }
    }
}
