import { Mongo } from 'meteor/mongo';

const Faculty = new Mongo.Collection('faculty');

export default Faculty;
